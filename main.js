// http://data.worldbank.org/indicator/NY.GDP.MKTP.CD?locations=US
// https://fred.stlouisfed.org/series/GOLDPMGBD228NLBM/downloaddata?cid=32217

function onGoldDataReady(goldData) {
    // d3.xml('gdp.xml', function (error, data) {
    //     var countries = {};
    //     var countryKeys = {};
    //     var records = data.querySelectorAll('record');
    //
    //     records.forEach(function (record) {
    //         var countryEl = record.querySelector('field[name="Country or Area"]');
    //         var countryName = countryEl.textContent;
    //         var countryKey = countryEl.getAttribute('key');
    //         countryKeys[countryKey] = countryName;
    //
    //         var year = +record.querySelector('field[name="Year"]').textContent;
    //         var value = +record.querySelector('field[name="Value"]').textContent;
    //         var countryData = countries[countryKey] = countries[countryKey] || {};
    //         var yearData = countryData[year] = countryData[year] || {};
    //         yearData.gdp = value;
    //     });
    //
    //     var bisectDate = d3.bisector(d => d.date).left;
    //
    //     var usaGdp = [];
    //     for (var year in countries.USA) {
    //         var gdp = countries.USA[year].gdp;
    //         if (gdp) {
    //             var date = new Date(year, 0);
    //             var goldDataIndex = bisectDate(goldData, date);
    //             var goldDataItem = goldData[goldDataIndex];
    //             usaGdp.push({
    //                 year: date,
    //                 gdp,
    //                 gdpInGold: gdp * (goldDataItem ? goldDataItem.usdInGold : 0)
    //             });
    //         }
    //     }
    //
    //     createTimeValueChart(usaGdp, {
    //         xField: 'year',
    //         yField: 'gdp',
    //         yFormat: '$.2s',
    //         title: 'United States GDP (USD)'
    //     });
    //     createTimeValueChart(usaGdp, {
    //         xField: 'year',
    //         yField: 'gdpInGold',
    //         yFormat: '$.2s',
    //         title: 'United States GDP (grams of gold)'
    //     });
    // });

    d3.xml('gdp_per_capita.xml', function (error, data) {
        var countries = {};
        var countryKeys = {};
        var records = data.querySelectorAll('record');

        records.forEach(function (record) {
            var countryEl = record.querySelector('field[name="Country or Area"]');
            var countryName = countryEl.textContent;
            var countryKey = countryEl.getAttribute('key');
            countryKeys[countryKey] = countryName;

            var year = +record.querySelector('field[name="Year"]').textContent;
            var value = +record.querySelector('field[name="Value"]').textContent;
            var countryData = countries[countryKey] = countries[countryKey] || {};
            var yearData = countryData[year] = countryData[year] || {};
            yearData.gdp = value;
        });

        function getCountryGdpData(country, goldData) {
            var gdpData = [];
            var bisectDate = d3.bisector(d => d.date).left;

            for (var year in country) {
                var gdp = country[year].gdp;
                if (gdp) {
                    var date = new Date(year, 0);
                    var goldDataIndex = bisectDate(goldData, date);
                    var goldDataItem = goldData[goldDataIndex];
                    gdpData.push({
                        year: date,
                        gdp,
                        gdpInGold: gdp * (goldDataItem ? goldDataItem.usdInGold : 0)
                    });
                }
            }
            return gdpData;
        }

        var usaGdp = getCountryGdpData(countries.USA, goldData);
        var ausGdp = getCountryGdpData(countries.AUS, goldData);
        var nldGdp = getCountryGdpData(countries.NLD, goldData);
        var gbrGdp = getCountryGdpData(countries.GBR, goldData);
        var rusGdp = getCountryGdpData(countries.RUS, goldData);
        var ukrGdp = getCountryGdpData(countries.UKR, goldData);

        createTimeValueChart(usaGdp, {
            xField: 'year',
            yField: 'gdp',
            yFormat: '$.2s',
            title: 'United States GDP per capita (USD)'
        });
        createTimeValueChart(usaGdp, {
            xField: 'year',
            yField: 'gdpInGold',
            yFormat: '.2f',
            title: 'United States GDP per capita (grams of gold)'
        });

        createTimeValueChart(ausGdp, {
            xField: 'year',
            yField: 'gdp',
            yFormat: '$.2s',
            title: 'Australia GDP per capita (USD)'
        });
        createTimeValueChart(ausGdp, {
            xField: 'year',
            yField: 'gdpInGold',
            title: 'Australia GDP per capita (grams of gold)'
        });

        createTimeValueChart(nldGdp, {
            xField: 'year',
            yField: 'gdp',
            yFormat: '$.2s',
            title: 'Netherlands GDP per capita (USD)'
        });
        createTimeValueChart(nldGdp, {
            xField: 'year',
            yField: 'gdpInGold',
            title: 'Netherlands GDP per capita (grams of gold)'
        });

        createTimeValueChart(gbrGdp, {
            xField: 'year',
            yField: 'gdp',
            yFormat: '$.2s',
            title: 'United Kingdom GDP per capita (USD)'
        });
        createTimeValueChart(gbrGdp, {
            xField: 'year',
            yField: 'gdpInGold',
            title: 'United Kingdom GDP per capita (grams of gold)'
        });

        createTimeValueChart(rusGdp, {
            xField: 'year',
            yField: 'gdp',
            yFormat: '$.2s',
            title: 'Russian Federation GDP per capita (USD)'
        });
        createTimeValueChart(rusGdp, {
            xField: 'year',
            yField: 'gdpInGold',
            title: 'Russian Federation GDP per capita (grams of gold)'
        });

        createTimeValueChart(ukrGdp, {
            xField: 'year',
            yField: 'gdp',
            yFormat: '$.2s',
            title: 'Ukraine GDP per capita (USD)'
        });
        createTimeValueChart(ukrGdp, {
            xField: 'year',
            yField: 'gdpInGold',
            title: 'Ukraine GDP per capita (grams of gold)'
        });
    });
}

d3.csv('gold/gold.csv', getGoldRow(), function (error, data) {
    var goldData = [];

    for (var i = 0, ln = data.length; i < ln; i++) {
        var item = data[i];

        if (item.date.getMonth() === 0) {
            item.usdInGold = 1 / item.price * 31.1034768;
            goldData.push(item);
        }
    }

    createTimeValueChart(goldData, {
        xField: 'date',
        yField: 'price',
        yFormat: '$.2f',
        title: '1 ounce of gold (USD)',
        notes: [
            {
                x: 1960,
                y: 100,
                text: 'Gold Standard (1944-1971)'
            }
        ]
    });

    createTimeValueChart(goldData, {
        xField: 'date',
        yField: 'usdInGold',
        title: '1 US Dollar (grams of gold)'
    });

    onGoldDataReady(goldData);
});

function getGoldRow() {
    var parseTime = d3.timeParse("%Y-%m-%d");
    return function goldRow(d) {
        return {
            date: parseTime(d.date),
            price: +d.price
        };
    };
}

function createTimeValueChart(data, opt) {
    var svgWidth = 640;
    var svgHeight = 480;
    var padding = {
        top: 40,
        right: 20,
        bottom: 40,
        left: 80
    };
    var width = svgWidth - padding.left - padding.right;
    var height = svgHeight - padding.top - padding.bottom;

    var x = d3.scaleTime().range([0, width]);
    var y = d3.scaleLinear().range([height, 0]);

    x.domain(d3.extent(data, d => d[opt.xField] )).nice();
    y.domain(d3.extent(data, d => d[opt.yField] )).nice();

    var xFormat = d3.timeFormat(opt.xFormat || '%Y');
    var yFormat = d3.format(opt.yFormat || '.2f');
    var xAxis = d3.axisBottom(x);
    var yAxis = d3.axisLeft(y)
        .ticks(10, yFormat)
        .tickSizeInner(-width);

    var line = d3.line()
        .x( d => x(d[opt.xField]) )
        .y( d => y(d[opt.yField]) );

    var svg = d3.select('body').append('p').append('svg')
        .attr('width', svgWidth)
        .attr('height', svgHeight);

    var context = svg.append('g')
        .attr('transform', 'translate(' + padding.left + ',' + padding.top + ')');

    if (opt.title) {
        svg.append('text')
            .attr('x', svgWidth / 2)
            .attr('y', padding.top / 2)
            .attr('class', 'title')
            .style('text-anchor', 'middle')
            .style('dominant-baseline', 'middle')
            .text(opt.title);
    }

    context.append('g')
        .attr('class', 'x axis')
        .attr('transform', 'translate(0,' + height + ')')
        .call(xAxis);

    context.append('g')
        .attr('class', 'y axis')
        .call(yAxis);

    context.append('path')
        .datum(data)
        .attr('class', 'line')
        .attr('d', line);

    if (opt.notes) {
        context.selectAll('.note').data(opt.notes).enter()
            .append('text')
            .attr('class', 'note')
            .attr('x', d => x(d.x instanceof Date ? d.x : new Date(d.x.toString())))
            .attr('y', d => y(d.y))
            .text(d => d.text);
    }

    var bisectDate = d3.bisector(d => d[opt.xField]).left;

    function crosshairLine(x1, y1, x2, y2) {
        return 'M' + x1 + ',' + y1 + 'L' + x2 + ',' + y2;
    }

    function sizeRect(rect, bbox) {
        rect
            .attr('x', bbox.x)
            .attr('y', bbox.y)
            .attr('width', bbox.width)
            .attr('height', bbox.height);
    }

    var overlay = context.append('g').attr('class', 'overlay');
    overlay.append('rect')
        .attr('width', width)
        .attr('height', height)
        .on('mouseover', () => crosshair.style('display', 'inline') )
        .on('mouseout', () => crosshair.style('display', 'none') )
        .on('mousemove', function() {
            var mouse = d3.mouse(this);

            var date = x.invert(mouse[0]);
            var i = bisectDate(data, date);

            if (i >= 0 && i < data.length && i !== crosshair.index) {
                crosshair.index = i;
                var item = data[i];
                var valueX = item[opt.xField];
                var valueY = item[opt.yField];
                var posX = x(valueX);
                var posY = y(valueY);
                crosshairX.attr('d', crosshairLine(posX, 0, posX, height));
                crosshairY.attr('d', crosshairLine(0, posY, width, posY));
                crosshairDot
                    .attr('cx', posX)
                    .attr('cy', posY);
                var xTipBBox = crosshairTipX.select('text')
                    .text(xFormat(valueX))
                    .attr('x', posX)
                    .node().getBBox();
                var yTipBBox = crosshairTipY.select('text')
                    .text(yFormat(valueY))
                    .attr('y', posY)
                    .node().getBBox();
                sizeRect(crosshairTipX.select('rect'), xTipBBox);
                sizeRect(crosshairTipY.select('rect'), yTipBBox);
            }
        });

    var crosshair = overlay.append('g').attr('class', 'crosshair');
    var crosshairX = crosshair.append('path').attr('class', 'x line');
    var crosshairY = crosshair.append('path').attr('class', 'y line');
    var crosshairDot = crosshair.append('circle').attr('class', 'dot');
    var crosshairTipX = crosshair.append('g').attr('class', 'x tip');
    crosshairTipX.append('rect');
    crosshairTipX.append('text')
        .attr('text-anchor', 'middle')
        .attr('alignment-baseline', 'hanging')
        .attr('y', height + xAxis.tickSize() * 1.5);
    var crosshairTipY = crosshair.append('g').attr('class', 'y tip');
    crosshairTipY.append('rect');
    crosshairTipY.append('text')
        .attr('text-anchor', 'end')
        .attr('alignment-baseline', 'middle')
        .attr('x', 0);
}
